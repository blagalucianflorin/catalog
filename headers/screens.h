#ifndef     _SCREENSH_
#define     _SCREENSH_

#include    "graphic.h"
#include    "cursor.h"
#include    "libft.h"

#include    <stdlib.h>
#include    <unistd.h>

color   backc;
int     error;

void    screen_intro ();
void    screen_main ();
void    screen_exit ();
void    screen_help ();
void    screen_help2 ();
color   backg ();
int     set_back (color col);
void    shadows ();
void    background ();

#endif
