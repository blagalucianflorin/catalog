#ifndef     _CURSORH_
#define     _CURSORH_

#include    <stdio.h>

typedef enum {RED, GREEN, YELLOW, BLUE, WHITE, BLACK} color;
typedef enum {UP, DOWN, RIGHT, LEFT} direction;

int     current_row;
int     temp_row;
color   col_front;
color   col_back;
color   back;

void    cursor_move (const direction dir, const int count);
void    cursor_position (const int row, const int col);
int     cursor_front_color (color col, int bright);
int     cursor_back_color (color col, int bright);
int     cursor_set_front (color col);
int     cursor_set_back (color col);
void    cursor_underline ();
void    cursor_bold_off ();
void    cursor_reset ();
color   cursor_front ();
color   cursor_back ();
void    cursor_bold ();
void    cursor_save ();
void    cursor_load ();
int     cursor_row ();

#endif
