#ifndef     _GRAPHICH_
#define     _GRAPHICH_

#include    <stdio.h>
#include    <stdlib.h>

#include    "window.h"
#include    "cursor.h"

void    line_string_3 (const char *s1, const char *s2, const char *s3, int row);
void    line_string_2 (const char *s1, const char *s2, int row);
void    line_string_left (const char *s, int row, int spaces);
void    print_inline_string (const char *s, const int width);
void    line_string_1 (const char *s, int row);
void    line_middle (int row);
void    line_bottom (int row);
void    line_input (int row);
void    line_empty (int row);
void    line_top (int row);
void    fullscreen_box ();
void    clear_screen ();

#endif
