#ifndef     _WINDOWH_
#define     _WINDOWH_

#include    <sys/ioctl.h>
#include    <stdio.h>
#include    <stdlib.h>

typedef struct  window
{
    short   height;
    short   width;
}               window;

window wnd;

void    window_initialize ();
short   window_height ();
short   window_width ();
int     check_w_size ();

#endif
