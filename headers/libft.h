#ifndef _LIBFTH_
#define _LIBFTH_

#include    <stdlib.h>

char	*ft_strcpy(char *d, const char *s);
int     ft_strcmp(const char *s1, const char *s2);
size_t	ft_strlen(const char *s);

#endif
