#ifndef _COMMANDSH_
#define _COMMANDSH_

#include    "cursor.h"
#include    "graphic.h"
#include    "window.h"
#include    "screens.h"
#include    "libft.h"

#include    <stdio.h>
#include    <stdlib.h>

void    command (const char *s);
void    c_help ();
void    c_help2 ();
void    c_exit ();
void    c_main ();
void    c_font_back ();
void    c_font_front ();
void    c_back ();
void    c_colors ();
void    c_reset ();
void    c_redraw ();

#endif
