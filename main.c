#include    <stdio.h>

#include    "headers/window.h"
#include    "headers/cursor.h"
#include    "headers/screens.h"

int main ()
{
    cursor_set_front (WHITE);
    cursor_set_back (BLUE);

    window_initialize ();

    screen_intro ();
    screen_main ();

    return (0);
}
