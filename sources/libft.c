#include    "../headers/libft.h"

char	*ft_strcpy(char *d, const char *s)
{
	char	*ptr;

	ptr = d;
	while (*ptr)
		*d++ = *s++;
	*d = '\0';
	return (ptr);
}

int		ft_strcmp(const char *s1, const char *s2)
{
	while(*s1 == *s2 && *s1 && *s2)
	{
		s1++;
		s2++;
	}
	if(!*s1 || !*s2)
		return (0);
	return ((int)(*s1 - *s2));
}

size_t	ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	while(*s++)
		i++;
	return (i);
}
