#include    "../headers/graphic.h"
#include    <string.h>

void    clear_screen ()
{
    system ("clear");
}

void    line_bottom (int row)
{
    int i;

    cursor_position (row, 0);
    cursor_bold ();
    printf ("%s", "╚");
    i = -1;
    while (++i < window_width () - 2)
        printf ("%s", "═");
    printf ("%s", "╝");
    cursor_bold_off ();

    return;
}

void    line_top (int row)
{
    int i;

    cursor_position (row, 0);
    cursor_bold ();
    printf ("%s", "╔");
    i = -1;
    while (++i < window_width () - 2)
        printf ("%s", "═");
    printf ("%s\n", "╗");
    cursor_bold_off ();

    return;
}

void    line_middle (int row)
{
    int i;

    cursor_position (row, 0);
    cursor_bold ();
    printf ("%s", "╠");
    i = -1;
    while (++i < window_width () - 2)
        printf ("%s", "═");
    printf ("%s\n", "╣");
    cursor_bold_off ();

    return;
}

void    line_empty (int row)
{
    int i;

    cursor_position (row, 0);
    cursor_bold ();
    printf ("%s", "║");
    i = -1;
    while (++i < window_width () - 2)
        printf (" ");
    printf ("%s\n", "║");
    cursor_bold_off ();

    return;
}

void    print_inline_string (const char *s, const int width)
{
    int string_len;
    int string_pos;
    int i;

    string_len = strlen (s);
    string_pos = width / 2 - string_len / 2 - 1;
    i = -1;
    cursor_bold ();
    while (++i < string_pos)
        printf (" ");
    printf ("%s", s);
    i += string_len - 1;
    while (++i < width)
        printf (" ");
    cursor_bold_off ();
}

void    line_string_1 (const char *s, int row)
{
    cursor_position (row, 0);
    cursor_bold ();
    printf ("║");
    print_inline_string (s, window_width () - 2);
    cursor_bold ();
    printf ("║\n");
    cursor_bold_off ();

    return;
}

void    line_string_2 (const char *s1, const char *s2, int row)
{
    int plus_width;

    cursor_position (row, 0);
    plus_width = (window_width () - 2) % 2;
    cursor_bold ();
    printf ("║");
    print_inline_string (s1, (window_width () - 2) / 2);
    print_inline_string (s2, (window_width () - 2) / 2 + plus_width);
    cursor_bold ();
    printf ("║\n");
    cursor_bold_off ();

    return;
}

void    line_string_3 (const char *s1, const char *s2, const char *s3, int row)
{
    int plus_width;

    cursor_position (row, 0);
    plus_width = (window_width () - 2) % 3;
    cursor_bold ();
    printf ("║");
    print_inline_string (s1, (window_width () - 2) / 3);
    print_inline_string (s2, (window_width () - 2) / 3 + plus_width);
    print_inline_string (s3, (window_width () - 2) / 3);
    cursor_bold ();
    printf ("║\n");
    cursor_bold_off ();

    return;
}

void    line_input (int row)
{
    int i;

    cursor_position (row, 0);
    cursor_bold ();
    printf ("╠══╡ ");
    cursor_front_color (WHITE, 1);
    cursor_back_color (BLACK, 1);
    cursor_save ();
    i = 4;
    while (++i < window_width () - 5)
        printf (" ");
    cursor_front_color (cursor_front (), 1);
    cursor_back_color (cursor_back (), 1);
    printf (" ╞══╣\n");
    cursor_bold_off ();
    cursor_load ();
}

void    fullscreen_box ()
{
    int i;

    line_top (1);
    i = 1;
    while (++i < window_height ())
        line_empty (i);
    line_bottom (window_height ());
}

void    line_string_left (const char *s, int row, int spaces)
{
    int i;
    int string_len;

    cursor_position (row, 0);
    cursor_bold ();
    printf ("║");
    i = -1;
    while (++i < spaces)
        printf (" ");
    printf ("%s", s);
    string_len = strlen (s);
    i = string_len + spaces;
    while (++i < window_width () - 1)
        printf (" ");
    printf ("║");
    cursor_bold_off ();

    return;
}
