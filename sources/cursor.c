#include    "../headers/cursor.h"
#include    "../headers/libft.h"

void    cursor_position (const int row, const int col)
{
    printf ("\033[%i;%iH", row + 1, col + 2);
    current_row = row;

    return;
}

int     cursor_front_color (color col, int bright)
{
    char s[2];

    if (bright == 1)
        ft_strcpy (s, ";1");
    else
        ft_strcpy (s, "\0\0");
    switch (col)
    {
        case RED:
            printf ("\033[31%sm", s);
            return (1);
        case GREEN:
            printf ("\033[32%sm", s);
            return (1);
        case YELLOW:
            printf ("\033[33%sm", s);
            return (1);
        case BLUE:
            printf ("\033[34%sm", s);
            return (1);
        case WHITE:
            printf ("\033[37%sm", s);
            return (1);
        case BLACK:
            printf ("\033[30%sm", s);
            return (1);
        default:
            return (0);
    }
}

int    cursor_back_color (color col, int bright)
{
    char s[2];

    if (bright == 1)
        ft_strcpy (s, ";1");
    else
        ft_strcpy (s, "\0\0");
    switch (col)
    {
        case RED:
            printf ("\033[41%sm", s);
            return (1);
        case GREEN:
            printf ("\033[42%sm", s);
            return (1);
        case YELLOW:
            printf ("\033[43%sm", s);
            return (1);
        case BLUE:
            printf ("\033[44%sm", s);
            return (1);
        case WHITE:
            printf ("\033[47%sm", s);
            return (1);
        break;
        case BLACK:
            printf ("\033[40%sm", s);
            return (1);
        default:
            return (0);
    }
}

void    cursor_reset ()
{
    printf ("\033[0m");

    return;
}

void    cursor_move (direction dir, int count)
{
    char    way;

    switch (dir) {
        case UP:
            way = 'A';
            if (current_row - count > 0)
                current_row -= count;
            else current_row = 0;
        break;
        case DOWN:
            way = 'B';
            current_row += count;
        break;
        case RIGHT:
            way = 'C';
        break;
        case LEFT:
            way = 'D';
        break;
    }
    printf ("\033[%i%c", count, way);

    return;
}

void    cursor_save ()
{
    printf ("\033[s");
    temp_row = current_row;

    return;
}

void    cursor_load ()
{
    printf ("\033[u");
    current_row = temp_row;

    return;
}

int     cursor_row ()
{
    return (current_row);
}

color   cursor_front ()
{
    return (col_front);
}

color   cursor_back ()
{
    return (col_back);
}

void    cursor_bold ()
{
    printf ("\033[1m");

    return;
}

void    cursor_bold_off ()
{
    printf ("\033[21m");

    return;
}

int     cursor_set_front (color col)
{
    if (cursor_front_color (cursor_front (), 1))
    {
        col_front = col;
        return (1);
    }
    else
        return (0);
}

int     cursor_set_back (color col)
{
    if (cursor_back_color (cursor_back (), 1))
    {
        col_back = col;
        return (1);
    }
    else
        return (0);
}
