#include    "../headers/screens.h"
#include    "../headers/commands.h"

#include    <string.h>

void    screen_intro ()
{
    set_back (WHITE);
    cursor_front_color (WHITE, 1);
    cursor_back_color (BLUE, 1);

    fullscreen_box ();
    shadows ();
    background ();
    line_string_1 ("Created by", window_height () / 2);
    cursor_bold ();
    line_string_1 ("Blaga Lucian Florin", window_height () / 2 + 1);
    cursor_bold_off ();
    sleep (3);

    return;
}

void    screen_main ()
{
    char s[100];

    fullscreen_box ();
    shadows ();
    background ();
    cursor_bold ();
    line_string_1 ("Home Page", 3);
    cursor_bold_off ();
    line_middle (window_height () - 4);
    strcpy (s, "no_error");
    line_string_left ("", window_height () - 3, 5);
    while (1)
    {
        if (!check_w_size ())
        {
            window_initialize ();

            screen_intro ();
            screen_main ();
        }
        cursor_back_color (cursor_back (), 1);
        cursor_front_color (cursor_front (), 1);
        line_empty (window_height () - 1);
        line_bottom (window_height ());
        shadows ();
        background ();
        command (s);
        line_input (window_height () - 2);
        scanf("%s", s);
    }
    cursor_back_color (cursor_back (), 1);
    cursor_front_color (cursor_front (), 1);

    return;
}

void    screen_exit ()
{
    fullscreen_box ();
    shadows ();
    background ();
    cursor_bold ();
    line_string_1 ("Thank you for using this program", window_height () / 2);
    cursor_bold_off ();
    sleep (3);
    cursor_reset ();
    cursor_position (window_height (), 0);
    printf ("\n");
    clear_screen ();
    exit (0);

    return;
}

void    screen_help ()
{
    int item_sp;

    item_sp = window_width () / 18;
    fullscreen_box ();
    shadows ();
    background ();
    line_middle (window_height () - 4);
    cursor_bold ();
    line_string_1 ("Commands Page 1", 3);
    cursor_bold_off ();
    line_string_left ("1. 'help'       - Show this page", 5, item_sp);
    line_string_left ("2. 'exit'       - Close the program", 6, item_sp);
    line_string_left ("3. 'home'       - Go to the home page", 7, item_sp);
    line_string_left ("4. 'colors'     - Show available colors", 8, item_sp);
    line_string_left ("5. 'background' - Change the background color"
                        , 9, item_sp);
    line_string_left ("6. 'font_fore'  - Change the font foreground"
                        , 10, item_sp);
    line_string_left ("7. 'font_back'  - Change the font background"
                        , 11, item_sp);
    line_string_left ("8. 'reset'      - Reset all settings"
                        , 12, item_sp);
    line_string_left ("9. 'redraw'     - Redraw the window"
                        , 13, item_sp);

    return;
}

void    screen_help2 ()
{
    int item_sp;

    item_sp = window_width () / 18;
    fullscreen_box ();
    shadows ();
    background ();
    line_middle (window_height () - 4);
    cursor_bold ();
    line_string_1 ("Commands Page 2", 3);
    cursor_bold_off ();
    line_string_left ("1. 'help'       - Show this page", 5, item_sp);

    return;
}

void    shadows ()
{
    int i;

    cursor_reset ();
    cursor_position (window_height () + 1, 0);
    cursor_back_color (backg (), 1);
    printf (" ");
    cursor_back_color (BLACK, 1);
    i = 0;
    while (++i < window_width () + 1)
        printf (" ");

    cursor_position (1, window_width ());
    cursor_back_color (backg (), 1);
    printf (" ");
    cursor_back_color (BLACK, 1);
    i = 0;
    while (++i < window_height ())
    {
        cursor_move (DOWN, 1);
        cursor_move (LEFT, 1);
        printf (" ");
    }

    cursor_front_color (cursor_front (), 1);
    cursor_back_color (cursor_back (), 1);
}

void    background ()
{
    int i;

    cursor_reset ();
    cursor_position (window_height () + 2, -2);
    cursor_back_color (backg (), 1);
    i = -1;
    while (++i < window_width () + 2)
        printf (" ");
    printf ("\033[0;%iH", window_width () + 5);
    i = 0;
    while (++i < window_height () + 3)
    {
        cursor_move (DOWN, 1);
        cursor_move (RIGHT, 1);
        printf (" ");
    }
    cursor_position (-1, -2);
    i = -1;
    while (++i < window_width () + 4)
        printf (" ");
    printf ("\033[0;0H");
    i = 0;
    while (++i < window_height () + 2)
    {
        cursor_move (DOWN, 1);
        cursor_move (LEFT, 1);
        printf (" ");
    }
    cursor_front_color (cursor_front (), 1);
    cursor_back_color (cursor_back (), 1);
}

int     set_back (color col)
{
    switch (col)
    {
        case RED:
            backc = RED;
            return (1);
        case GREEN:
            backc = GREEN;
            return (1);
        case YELLOW:
            backc = YELLOW;
            return (1);
        case BLUE:
            backc = BLUE;
            return (1);
        case WHITE:
            backc = WHITE;
            return (1);
        default:
            return (0);
    }
}

color   backg ()
{
    return (backc);
}
