#include    "../headers/commands.h"

void    command (const char *s)
{
    if (!ft_strcmp (s, "exit"))
    {
        c_exit ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "help2") && ft_strlen (s) == 5)
    {
        c_help2 ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "help") && ft_strlen (s) == 4)
    {
        c_help ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "home"))
    {
        c_main ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "redraw"))
    {
        c_redraw ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "font_fore"))
    {
        c_font_front ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "font_back"))
    {
        c_font_back ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "background"))
    {
        c_back ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "colors"))
    {
        c_colors ();
        error = 0;
        return;
    }
    if (!ft_strcmp (s, "reset"))
    {
        c_reset ();
        error = 0;
        return;
    }
    if (error)
    {
        line_string_left ("Unkown command. You can try using 'help'",
                    window_height () - 3, 1);
        error = 0;
        return;
    }
        error = 1;

    return;
}

void    c_exit ()
{
    screen_exit ();

    return;
}

void    c_help ()
{
    screen_help ();

    return;
}

void    c_help2 ()
{
    screen_help2 ();

    return;
}

void    c_main ()
{
    screen_main ();

    return;
}

void    c_font_back ()
{
    char    s[100];

    while (ft_strcmp (s, "cancel"))
    {
        cursor_front_color (cursor_front (), 1);
        cursor_back_color (cursor_back (), 1);
        line_string_left ("Enter a color for the font background: (or 'cancel')"
                            ,window_height () - 3, 1);
        line_input (window_height () - 2);
        scanf ("%s", s);
        fflush(stdin);
        if (!ft_strcmp (s, "red"))
        {
            cursor_set_back (RED);
            break;
        }
        if (!ft_strcmp (s, "green"))
        {
            cursor_set_back (GREEN);
            break;
        }
        if (!ft_strcmp (s, "yellow"))
        {
            cursor_set_back (YELLOW);
            break;
        }
        if (!ft_strcmp (s, "blue"))
        {
            cursor_set_back (BLUE);
            break;
        }
        if (!ft_strcmp (s, "white"))
        {
            cursor_set_back (WHITE);
            break;
        }
    }
    cursor_back_color (cursor_back (), 1);
    cursor_front_color (cursor_front (), 1);
    line_empty (window_height () - 3);
    error = 0;
    screen_main ();

    return;
}

void    c_font_front ()
{
    char    s[100];

    while (ft_strcmp (s, "cancel"))
    {
        cursor_front_color (cursor_front (), 1);
        cursor_back_color (cursor_back (), 1);
        line_string_left ("Enter a color for the font foreground: (or 'cancel')"
                            ,window_height () - 3, 1);
        line_input (window_height () - 2);
        scanf ("%s", s);
        fflush(stdin);
        if (!ft_strcmp (s, "red"))
        {
            cursor_set_front (RED);
            break;
        }
        if (!ft_strcmp (s, "green"))
        {
            cursor_set_front (GREEN);
            break;
        }
        if (!ft_strcmp (s, "yellow"))
        {
            cursor_set_front (YELLOW);
            break;
        }
        if (!ft_strcmp (s, "blue"))
        {
            cursor_set_front (BLUE);
            break;
        }
        if (!ft_strcmp (s, "white"))
        {
            cursor_set_front (WHITE);
            break;
        }
    }
    cursor_back_color (cursor_back (), 1);
    cursor_front_color (cursor_front (), 1);
    line_empty (window_height () - 3);
    error = 0;
    screen_main ();

    return;
}

void    c_back ()
{
    char    s[100];

    while (ft_strcmp (s, "cancel"))
    {
        cursor_front_color (cursor_front (), 1);
        cursor_back_color (cursor_back (), 1);
        line_string_left ("Enter a color for the background: (or 'cancel')",
                    window_height () - 3, 1);
        line_input (window_height () - 2);
        scanf ("%s", s);
        fflush(stdin);
        if (!ft_strcmp (s, "red"))
        {
            set_back (RED);
            break;
        }
        if (!ft_strcmp (s, "green"))
        {
            set_back (GREEN);
            break;
        }
        if (!ft_strcmp (s, "yellow"))
        {
            set_back (YELLOW);
            break;
        }
        if (!ft_strcmp (s, "blue"))
        {
            set_back (BLUE);
            break;
        }
        if (!ft_strcmp (s, "white"))
        {
            set_back (WHITE);
            break;
        }
    }
    cursor_back_color (cursor_back (), 1);
    cursor_front_color (cursor_front (), 1);
    line_empty (window_height () - 3);
    background ();
    shadows ();

    return;
}

void    c_colors ()
{
    line_string_left ("Available colors: red/green/yellow/blue/white" ,
                        window_height () - 3, 1);
    return;
}

void    c_reset ()
{
    cursor_set_front (WHITE);
    cursor_set_back (BLUE);
    set_back (WHITE);
    background ();
    shadows ();
    line_empty (window_height () - 3);
    error = 0;
    screen_main ();
}

void    c_redraw ()
{
    window_initialize ();

    screen_intro ();
    screen_main ();

    return;
}
