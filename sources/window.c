#include    "../headers/window.h"

void    window_initialize ()
{
    struct winsize w;

    ioctl(0, TIOCGWINSZ, &w);
    wnd.height   = w.ws_row - 3;
    wnd.width    = w.ws_col - 3;
    system ("clear");
}

short   window_height ()
{
    return (wnd.height);
}

short   window_width ()
{
    return (wnd.width);
}

int     check_w_size ()
{
    struct  winsize w;

    ioctl(0, TIOCGWINSZ, &w);
    if (w.ws_row - 3 == wnd.height && w.ws_col - 3 == wnd.width)
        return (1);
    else
        return (0);
}
